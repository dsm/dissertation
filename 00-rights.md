<#ifndef BOOK>
---
title: "License & Attribution"
---
<#endif>
# License and Attribution {epub:type=copyright-page class="hideme"}

![](fig/pdmark.svg){.pdmark}

This dissertation is dedicated to the public domain through a [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/) license by the author and creator, [David Samuel Meyer](https://dsm.namu.blue). All images and figures are also in the public domain.

The source for this project is available at <https://codeberg.org/dsm/dissertation>
