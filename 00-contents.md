```{=commonmark}
This is a copy of my thesis as a single webpage. A [pdf version](/thesis/learning_as_inhabitation.pdf) and [an ebook](/thesis/learning_as_inhabitation.epub) are also available. Each chapter can also be viewed [on its own page](/thesis). The [original version](/thesis/learning_as_inhabitation-original.pdf) submitted to Yonsei University is also available, but the revised version should be preferred since it includes typo corrections and formatting adjustments that improve the reading experience.
```


### Contents

- [Abstract](#abstract)
- [Acknowledgements](#acknowledgements)
- [1. Introduction: The Need for an Eco-centric Re-conception of
  Learning](#introduction-the-need-for-an-eco-centric-re-conception-of-learning)
- [2. A Survey of Ecological
  Humanism](#a-survey-of-ecological-humanism)
  - [The Human Eros & Vita
    Humana](#the-human-eros-vita-humana)
  - [Spiritual
    Ecologies](#spiritual-ecologies)
  - [Eco-ontology](#eco-ontology)
- [3. Dewey's Reconstruction of
  Metaphysics](#deweys-reconstruction-of-metaphysics)
  - [Social Interactivity & the Metaphysical
    Map](#social-interactivity-the-metaphysical-map)
  - [The Problematic of Continuity in Dewey's Cultural
    Naturalism](#the-problematic-of-continuity-in-deweys-cultural-naturalism)
- [4. A Nature-Prime Metaphysics of
  Learning](#a-nature-prime-metaphysics-of-learning)
  - [Immediacy & Quality](#immediacy-quality)
  - [Situations as Primary
    Realities](#situations-as-primary-realities)
  - [Imagination, Appreciation &
    *In*-habitation](#imagination-appreciation-in-habitation)
  - [Interest, Individuality &
    Temporality](#interest-individuality-temporality)
  - [Growth & Culture as
    Transactional](#growth-culture-as-transactional)
- [5. The Philosophical Foundations of Inhabitation in
  Experience](#the-philosophical-foundations-of-inhabitation-in-experience)
  - [Philosophy as Art](#philosophy-as-art)
  - [The Denotative-Empirical
    Method](#the-denotative-empirical-method)
  - [Philosophy as the Study of
    Life-Experience](#philosophy-as-the-study-of-life-experience)
  - [Critique of Value](#critique-of-value)
  - [Wisdom & the Worth of
    Wondering](#wisdom-the-worth-of-wondering)
- [6. Learning & Living as
  Art](#learning-living-as-art)
  - [Continuity, Aesthetic &
    Art](#continuity-aesthetic-art)
  - [Learning as Aesthetic Appreciation &
    Production](#learning-as-aesthetic-appreciation-production)
  - [Learning & the Common
    Aesthetic](#learning-the-common-aesthetic)
- [7. The Learning
  Situation](#the-learning-situation)
  - [Transmission, Facilitation &
    Transaction](#transmission-facilitation-transaction)
  - [Learning & Teaching as Transactional Phases of Learning
    Situations](#learning-teaching-as-transactional-phases-of-learning-situations)
  - [Democracy & the Learning
    Community](#democracy-the-learning-community)
- [8. Conclusion: Learning Just
  Because](#conclusion-learning-just-because)
- [Appendix I: The Red
  Wheelbarrow](#appendix-i-the-red-wheelbarrow)
- [Appendix II: Sea
  Calm](#appendix-ii-sea-calm)
- [References](#references)
- [국문요약](#국문요약)

```{=commonmark}
* * *

<p  style="text-align: center;"><small>Dedicated <br>to the living memory of<br>
<strong>Betty Lou Meyer</strong><br>
<strong>Joe David Meyer</strong><br>
&<br>
<strong>도휘자</strong></small></p>

* * *

```
