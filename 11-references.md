<#ifndef BOOK>
---
title: References
description: Bibliography of Learning as Inhabitation
weight: 110
---
<#endif>

<#ifdef TEX>
\\clearpairofpagestyles
\\ohead{References}
\\cfoot[\\pagemark]{\\pagemark}
<#endif>

<#ifndef NOH1># References {- epub:type=bibliography}<#endif>

<div id="refs"></div>
