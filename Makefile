TEMPLATES=templates
SRC=.
BUILD=blog
DATE=`date "+%B %d, %Y"`
#BIB=$$HOME/study/lib/index.bib
#CSL=$$HOME/study/lib/zotero/styles/chicago-author-date.csl
BIB=ref/references.bib
CSL=ref/chicago-author-date.csl
FRONT=00-abstract.md \
      00-acknowledgements.md
MAIN=01-intro.md \
			02-ecoontology.md \
			03-deweys_metaphysics.md \
			04-metaphysics_of_learning.md \
			05-philosophical_foundations.md \
			06-learning_as_art.md \
			07-learning_situation.md \
			08-conclusion.md
APPEND=09-appendix1.md \
			10-appendix2.md
BACK=11-references.md \
			12-초록.md
BODY=$(MAIN) $(APPEND) $(BACK)

EPUB_FRONT=00-rights.md 00-dedication.md

TS=`git log -1 --pretty=format:%ci -- $<`
H=`git log -1 --pretty=format:%H -- $<`

REQS=$(SRC)/%.md metadata.yml macros.gpp
PDF_NAME=learning_as_inhabitation
PDF_REQS=$(PDF_NAME).tex $(PDF_NAME).aux $(PDF_NAME).lof $(PDF_NAME).toc $(PDF_NAME).log $(PDF_NAME).bcf $(PDF_NAME).run.xml

BOOK_OPTS = \
            --metadata-file=book.yml \
            --file-scope \

COMMON_OPTS = \
              --metadata-file=metadata.yml \
              -f markdown \
              --filter pandoc-xnos \
              --filter pandoc-fignos \
              --bibliography=$(BIB) \
		          -C \
              --output $@

STANDALONE_OPTS = \
              -M lastmod="$$TIMESTAMP" \
              -M revhist="https://codeberg.org/dsm/dissertation/commits/branch/main/$<" \
              -M lastrev="https://codeberg.org/dsm/dissertation/commit/$$HASH" \


CMARK_OPTS =  \
    --standalone \
    -t commonmark_x-fenced_divs+implicit_figures \
    $(COMMON_OPTS) \


prep = rm -f "$(1).buildme"; gpp -o "$(1).buildme" -H --include macros.gpp ${foreach opt,$(2),-D$(opt)=1} "$(1)"

CLEANUP = rm -f *.buildme


$(BUILD)/book.md: 00-contents.md $(FRONT) $(BODY) metadata.yml book.yml macros.gpp
	for d in 00-contents.md $(FRONT) $(BODY); do $(call prep,$$d,CMARK BOOK); \
  done

	pandoc $(foreach doc,00-contents.md $(FRONT) $(BODY), "$(doc).buildme") \
  $(CMARK_OPTS) \
  $(BOOK_OPTS)

	$(CLEANUP)

$(BUILD)/%.md: $(REQS)
	@$(call prep,$<,CMARK NOH1 MONO)

	TIMESTAMP=$(TS); \
  HASH=$(H); \
  pandoc \
  $(STANDALONE_OPTS) \
  $(CMARK_OPTS) \
  $<.buildme

	$(CLEANUP)

blog:
	for PAGE in $(FRONT); do \
    make "$(BUILD)/$$PAGE";\
  done

	for CHAPTER in $(BODY); do \
    make "$(BUILD)/$$CHAPTER";\
  done


$(BUILD)/$(PDF_NAME).pdf: $(PDF_NAME).tex

	make $(PDF_NAME).tex

	xelatex $(PDF_NAME); xelatex $(PDF_NAME)

	mv $(PDF_NAME).pdf $(BUILD)/

	rm -f $(PDF_REQS)

$(PDF_NAME).tex: $(BODY) metadata.yml tex.yml book.yml macros.gpp
	for d in $(BODY); do $(call prep,$$d,BOOK TEX); \
  done

	rm -f $(PDF_REQS)

	pandoc $(foreach doc,$(BODY), "$(doc).buildme") \
		--template="$(TEMPLATES)"/template.tex \
    -t latex+smart \
    --metadata-file=tex.yml \
    $(BOOK_OPTS) \
    $(COMMON_OPTS) \
    -N \
    -M date="December 2021" \
    -M timestamp="$(DATE)" \
    -s \
		--toc

	$(CLEANUP)

$(BUILD)/learning_as_inhabitation.epub: $(EPUB_FRONT) $(FRONT) $(BODY) metadata.yml book.yml epub-metadata.yml macros.gpp
	for d in $(EPUB_FRONT) $(FRONT) $(BODY); do $(call prep,$$d,BOOK EPUB); \
  done

	pandoc $(foreach doc,$(EPUB_FRONT) $(FRONT) $(BODY), "$(doc).buildme") \
    $(COMMON_OPTS) \
    $(BOOK_OPTS) \
    --metadata-file=epub-metadata.yml \
		--template="$(TEMPLATES)"/template.epub \
		-c "$(TEMPLATES)"/epub.css \
    --standalone \
		--file-scope \
		--toc

	$(CLEANUP)


help:
	@echo ''
	@echo 'Build options for: "Learning as Inhabitation"'
	@echo ''
	@echo 'Usage:'
	@echo '   make help             show this message'
	@echo '   make pdf              build a fancy pdf'
	@echo '   make epub             build epub format'
	@echo '   make blog             build markdown for hugo blog'
	@echo '   make all              build all formats'

all: $(BUILD)/learning_as_inhabitation.pdf $(BUILD)/learning_as_inhabitation.epub blog $(BUILD)/book.md

.PHONY: help blog
