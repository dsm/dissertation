<#ifndef BOOK>
---
title: Dedication
description: Dedication page of Learning an Inhabitation
---
<#endif>
<#ifndef NOH1># Dedication {- epub:type=dedication class="hideme"}<#endif>
<p class="dedication">
|
|
| Dedicated to the living memory of
| Betty Lou Meyer
| Joe David Meyer
| &
| 도휘자
</p>
