<#ifndef BOOK>
---
title: "Appendix II"
subtitle: "Sea Calm"
title-prefix: II
weight: 100
description: Second of two appendices in Learning as Inhabitation.
summary: A well-known poem by Langston Hughes
tags:
- poetry
---
<#endif>
<#ifndef NOH1># Appendix II: Sea Calm {- epub:type=appendix}<#endif>

<#ifdef TEX>\\singlespacing<#endif>

| "Sea Calm" 
|    by Langston Hughes
|    from *The Weary Blues*, 1926
|
|
|
| <#ifdef TEX>\\noindent <#endif>How still,
| How strangely still
| The water is today,
| It is not good
| For water
| To be still that way.
