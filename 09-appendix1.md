<#ifndef BOOK>
---
title: "Appendix I"
subtitle: "The Red Wheelbarrow"
title-prefix: I
weight: 90
description:  One of two appendices to Learning as Inhabitation.
summary: A well-know poem by William Carlos Williams
tags:
- poetry
---
<#endif>
<#ifndef NOH1># Appendix I: The Red Wheelbarrow {.unnumbered epub:type=appendix #appendix-i-the-red-wheelbarrow}<#endif>

<#ifdef TEX>
\\singlespacing<#endif>

| "The Red Wheelbarrow" 
|    by William Carlos Williams
|    from *Spring and All*, 1923
|
|
|
| <#ifdef TEX>\\noindent <#endif>so much depends
| upon
| 
| a red wheel
| barrow
| 
| glazed with rain
| water
| 
| beside the white
| chickens
