
| Dedicated to the living memory of
| Betty Lou Meyer
| Joe David Meyer
| &
| 도휘자

-   [Acknowledgements](00-acknowledgements)
-   [Abstract](00-abstract)
-   [Introduction: The Need for an Eco-centric Re-conception of
    Learning](01-intro)
-   [A Survey of Ecological Humanism](02-ecoontology)
    -   [The Human Eros & Vita
        Humana](02-ecoontology#the-human-eros-vita-humana)
    -   [Spiritual Ecologies](02-ecoontology#spiritual-ecologies)
    -   [Eco-ontology](02-ecoontology#eco-ontology)
-   [Dewey's Reconstruction of Metaphysics](03-deweys_metaphysics)
    -   [Social Interactivity & the Metaphysical
        Map](03-deweys_metaphysics#social-interactivity-the-metaphysical-map)
    -   [The Problematic of Continuity in Dewey's Cultural
        Naturalism](03-deweys_metaphysics#the-problematic-of-continuity-in-deweys-cultural-naturalism)
-   [A Nature-Prime Metaphysics of
    Learning](04-metaphysics_of_learning)
    -   [Immediacy &
        Quality](04-metaphysics_of_learning#immediacy-quality)
    -   [Situations as Primary
        Realities](04-metaphysics_of_learning#situations-as-primary-realities)
    -   [Imagination, Appreciation &
        *In*-habitation](04-metaphysics_of_learning#imagination-appreciation-in-habitation)
    -   [Interest, Individuality &
        Temporality](04-metaphysics_of_learning#interest-individuality-temporality)
    -   [Growth & Culture as
        Transactional](04-metaphysics_of_learning#growth-culture-as-transactional)
-   [The Philosophical Foundations of Inhabitation in
    Experience](05-philosophical_foundations)
    -   [Philosophy as
        Art](05-philosophical_foundations#philosophy-as-art)
    -   [The Denotative-Empirical
        Method](05-philosophical_foundations#the-denotative-empirical-method)
    -   [Philosophy as the Study of
        Life-Experience](05-philosophical_foundations#philosophy-as-the-study-of-life-experience)
    -   [Critique of
        Value](05-philosophical_foundations#critique-of-value)
    -   [Wisdom & the Worth of
        Wondering](05-philosophical_foundations#wisdom-the-worth-of-wondering)
-   [Learning & Living as Art](06-learning_as_art)
    -   [Continuity, Aesthetic &
        Art](06-learning_as_art#continuity-aesthetic-art)
    -   [Learning as Aesthetic Appreciation &
        Production](06-learning_as_art#learning-as-aesthetic-appreciation-production)
    -   [Learning & the Common
        Aesthetic](06-learning_as_art#learning-the-common-aesthetic)
-   [The Learning Situation](07-learning_situation)
    -   [Transmission, Facilitation &
        Transaction](07-learning_situation#transmission-facilitation-transaction)
    -   [Learning & Teaching as Transactional Phases of Learning
        Situations](07-learning_situation#learning-teaching-as-transactional-phases-of-learning-situations)
    -   [Democracy & the Learning
        Community](07-learning_situation#democracy-the-learning-community)
-   [Conclusion: Learning Just Because](08-conclusion)
-   [Appendix I: The Red Wheelbarrow](09-appendix1)
-   [Appendix II: Sea Calm](10-appendix2)
-   [국문요약](12-abstract)
