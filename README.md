# Learning as Inhabitation: A Reinterpretation of Dewey's Concept of Learning through Alexander's Ecological Humanism

This repository contains all the source files for my dissertation. The text itself is written in markdown, now with a bunch of other gubbins in there to facilitate the production of multiple formats. It is converted to pdf, epub, and hugo-friendly commonmark using pandoc. It may be of interest to anyone attempting to write their dissertation using a similar workflow. Admittedly, this is a crazy hot mess. It uses gpp to preprocess the files, which is really something I should be doing with a lua filter. The gpp method is quick and relatively easy, so it will do for now.

## License

The published version of this dissertation was somewhat awkwardly licensed under a CC-BY-NC 2.0 South Korea license, but the source files and alternative builds of the text found in this repo are dedicated to the public domain via CC0 1.0 Universal. The [CSL files](ref/chicago-author-date.csl) included in this repo are licensed under a **CC-BY-SA 3.0** license. The [included images](fig) are public domain, and the remaining scripts, templates, etc. are licensed as [**Beer-Ware**](LICENSE).

## Contents

The plain text of the dissertation may be viewed right here in this repo. A pdf copy of the final manuscript is available, along with some other formats on [my homepage](https://dsm.namu.blue/thesis).

1. [Introduction: The Need for an Eco-centric Re-conception of Learning](01-intro.md)
2. [A Survey of Ecological Humanism](02-ecoontology.md)
3. [Dewey's Reconstruction of Metaphysics](03-deweys_metaphysics.md)
4. [A Nature-Prime Metaphysics of Learning](04-metaphysics_of_learning.md)
5. [The Philosophical Foundations of Inhabitation in Experience](05-philosophical_foundations.md)
6. [Learning & Living as Art](06-learing_as_art.md)
7. [The Learning Situation](07-learning_situation.md)
8. [Conclusion: Learning Just Because](08-conclusion.md)
9. [Appendix I: The Red Wheelbarrow](09-appendix1.md)
10. [Appendix II: Sea Calm](10-appendix2.md)

## Abstract

This paper examines the concept of learning through the eco-centric, or nature-prime, philosophies of John Dewey and Thomas Alexander. In particular, it interprets learning from the perspective of nature itself as inhabitation, which locates the process and activity of learning within the transactivity of learning situations themselves. This research is a response to the increasingly urgent need for human beings to re-evaluate our relationship with nature for a more ecologically conscious and responsible inhabitation of our world. Ecological humanism is particularly relevant to this end due to the nature-prime or eco-ontological orientation of its philosophies. The concept of learning as the inhabitation of transactional wholes, or situations, contributes such an ecological humanistic perspective to a niche area of research in the philosophy of education which aims to ecologize or de-anthropocentrize educational theory and practice.

Through a review of the corpora of Dewey and Alexander and other related literature, this paper examines the relevant philosophical and metaphysical issues involved in learning and inhabitation from the point of view of ecological humanism. In particular it aims to 1) examine the main philosophical and metaphysical points of this eco-centric concept of learning, 2) discuss the vital intersections of art and philosophy with learning, as well as the significance of meaning, value, interest, and wisdom in that process of inhabitation, 3) demonstrate the generality and fundamentally autotelic nature of learning as the life process itself, 4) and articulate how learning so conceived as a direct participation in the growth of and communion in a world or ecosystem discloses possibilities for more ecologically fluent ways of living together, among all existences, and what implications this has for human inhabitation and education in particular.

The main positions of Alexander's ecological humanism are surveyed, followed by an in-depth review of Dewey's reconstruction of metaphysics with special emphasis on his principle of continuity. Natural continuity is then discussed in the metaphysics of experience and learning, demonstrating how learning is the growth of situations through the realization of individual interest. This process is fundamentally aesthetic in nature. Philosophy serves as a method of remaining aesthetically receptive to our world to critically evaluate how we inhabit it. Learning is itself art, or, a process of aesthetic appreciation and production, through which we participate most vitally and directly in a communion with our cultural and natural world.

From this point of view, the commonplace dualisms of contemporary education are critiqued, and the import of a transactional, learning-centric paradigm of education is discussed. Namely, it is argued that learning, understood as inhabitation, is the life process itself, and is fundamentally autotelic in nature. Understood in this way, learning represents our most basic point of contact and expression of the world or ecosystem. It is concluded that not only must learning situations be allowed to determine their own meanings to as great a degree as possible, but also that this is a condition for a democratic and ecologically conscientious education and inhabitation of the Earth.

## 국문요약

**서식(棲息)으로서의 배움: Alexander의 생태학적 휴머니즘에 비추어 보는 Dewey의 배움 개념**

본 논문은 John Dewey와 Thomas Alexander의 생태 중심적 철학에 비추어보는 배움의 의미를 살펴본다. 특히, "배우는 상황" 그 자체의 교호작용을 배움의 주체로 보는 이른바 "서식(棲息)으로서의 배움" 개념을 제시한다. 본 연구는 생태에 대한 의식이 높아지는 서식을 위하여 자연과 인간의 관계를 재고찰하는 과제의 긴급한 과제에 대응하고자 한다. 이러한 과제의 특성에 따라 생태학적 휴머니즘의 자연 중심적 혹은 생태존재론적(eco-ontological) 특별한 주목을 받을 만한 가치가 있다. "배우는 상황" 그 자체의 서식으로 보는 배움 개념은 교육 이론과 실제를 생태화하거나 인류중심적인 성향에서 벗어나기 위한 교육철학 연구 동향에 이러한 생태학적 휴머니즘적인 관점을 기여한다. 

Dewey와 Alexander의 저서 및 관련 문헌을 검토함으로써 배움을 서식으로 재해석하는 본 연구의 목적은 1) 생태 중심적인 배움 개념의 철학적 또는 형이상학적 요점을 정리하고 2) 배움, 예술 그리고 철학의 교차성을 드러내며 이러한 배움 개념에 비추어 보는 의미, 가치, 흥미 그리고 지혜의 의의를 살펴보고 3) 사는 과정 그 자체로 보는 배움의 보편성 및 자기목적적인(autotelic) 특성을 밝히고 4) 이러한 배움의 개념이 생태의 성장과 교감에 직접적으로 참여함으로써 보다 생태적으로 충실한 서식의 가능성과 장애물을 드러내며 교육과 사회에 대한 그것의 함의를 논하고자 한다.

Alexander의 생태학적 휴머니즘의 주요 개념과 입장을 정리한 다음에 Dewey의 연속성 개념을 중심으로 Dewey가 재구성한 형이상학을 자세히 살펴본다. 이어서 경험과 배움의 형이상학에 대한 자연적 연속성의 의미를 해석한다. 개별적 흥미의 실현으로 이루어지는 상황의 성장 그 자체를 배움으로 보는 관점을 드러내고자 한다. 이러한 과정이 근본적으로 심미적이라는 점에 주목한다. 철학적 탐구는 세상에 대한 심미적 감수성을 보존하면서 그 세계에 처하는 우리의 사는 방식 혹은 서식 과정에 비평적으로 접근하기 위한 활동이 된다. 따라서 배움은 "심미적 감상과 창조"로 이해되는 하나의 예술이 되고 자연적 또는 문화적인 세계에 가장 직접적으로 관여하고 교감하는 과정이 된다고 논의한다.

이러한 관점에 입각하여 현대 교육 담론에서 흔히 나타나는 이분법들을 비평하고 교호작용적이며 배움 중심적인 교육 패러다임의 가능성과 의의를 논한다. 서식으로 이해되는 배움이 사는 과정 그 자체로서 근본적으로 자기 목적적인 과정이며, 세상 혹은 생태와의 제일 보편적인 교감과 관여가 된다는 점에 특별히 주목한다. 결론적으로는, 소위 "배우는 상황"이 스스로의 뜻, 목적, 그리고 방법을 스스로 결정할 수 있어야 하며, 이는 지구상에서의 생태적으로 성실하며 민주주의적인 서식을 위한 조건이라고 주장한다.
