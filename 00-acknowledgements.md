<#ifndef BOOK>
---
title: Acknowledgements
title-prefix: i
weight: 5
description: Acknowledgements for Learning as Inhabitation
summary: I am indebted to numerous people through whose support, guidance, and love this dissertation has been made possible.
---
<#endif>
<#ifndef NOH1># Acknowledgements {- epub:type=acknowledgements}<#endif>

I am indebted to numerous people through whose support, guidance, and love this dissertation has been made possible.

I would like to thank my thesis supervisor, Dr. Keumjoong Hwang, and the professors on the committee for this dissertation, Dr. Sang-O Lee, Dr. Soon-Yong Park, Dr. Duck-Joo Kwak, and Dr. Jeong-Gil Woo, for their patience, insight, and dedication throughout the review process. 

I would like to thank my grandmother, Betty Lou Meyer, for her unconditional love and encouragement, and my parents, Joe David Meyer and Kathleen Marie Mitchell, for inspiring me to dream and wonder---even when this took me so far away from home---and for instilling in me at a young age a lifelong love of learning.

I would like to thank my mother and father-in-law, Sun-Hwa Kim and Yang-Jin Kim, without whose support and love this dissertation would have never been completed.

I want to thank my wife, Jihyun Kim, for her patience, support, and love; for sharing in this adventure with me and being my foundation throughout this process---especially during the pandemic no less!

I also want to thank our children, Mindeulle, Mirinae, and Hanee, from whose very existence in this world I have learned so much, and with whom I look forward to continue growing and wondering.

I would like to thank Erik Hanson and Loren Goodman for intermittent moral support and comic relief.

I would like to thank Sci-Hub, Philpapers.org, Project Gutenberg, and the Internet Archive for providing access to the literature and media referenced in this paper.

And finally, I would like to thank the many educators who have influenced and supported me in some way throughout my life; Lisa Whitehorn, Maureen Clements, Martine Johnson, Roy Reddin, Eric Tofflemire, David Alred, Peggy Herndon, Normetta Muir, Madeeha Pitcher, James Horton, Keith Foust, Keith Brookshaw, Kumu Lehua Vincent, Michelle Ebersole, & Christopher Reichl
