<#ifndef BOOK>
---
title: Abstract
title-prefix: ii
weight: 1
description: Abstract of Learning as Inhabitation
summary: This paper examines the concept of learning through the eco-centric, or nature-prime, philosophies of John Dewey and Thomas Alexander. In particular, it interprets learning from the perspective of nature itself as inhabitation, which locates the process and activity of learning within the transactivity of learning situations themselves.
tags:
- John Dewey
- Thomas Alexander
- Inhabitation
- Aesthetic Experience
- Autotelic Learning
- Ecological Humanism
- Transaction
- Learning Situation
---
<#endif>
<#ifndef NOH1># Abstract {- epub:type=abstract}<#endif>

This paper examines the concept of learning through the eco-centric, or nature-prime, philosophies of John Dewey and Thomas Alexander. In particular, it interprets learning from the perspective of nature itself as inhabitation, which locates the process and activity of learning within the transactivity of learning situations themselves. This research is a response to the increasingly urgent need for human beings to re-evaluate our relationship with nature for a more ecologically conscious and responsible inhabitation of our world. Ecological humanism is particularly relevant to this end due to the nature-prime or eco-ontological orientation of its philosophies. The concept of learning as the inhabitation of transactional wholes, or situations, contributes such an ecological humanistic perspective to a niche area of research in the philosophy of education which aims to ecologize or de-anthropocentrize educational theory and practice.

Through a review of the corpora of Dewey and Alexander and other related literature, this paper examines the relevant philosophical and metaphysical issues involved in learning and inhabitation from the point of view of ecological humanism. In particular it aims to 1) examine the main philosophical and metaphysical points of this eco-centric concept of learning, 2) discuss the vital intersections of art and philosophy with learning, as well as the significance of meaning, value, interest, and wisdom in that process of inhabitation, 3) demonstrate the generality and fundamentally autotelic nature of learning as the life process itself, 4) and articulate how learning so conceived as a direct participation in the growth of and communion in a world or ecosystem discloses possibilities for more ecologically fluent ways of living together, among all existences, and what implications this has for human inhabitation and education in particular.

The main positions of Alexander's ecological humanism are surveyed, followed by an in-depth review of Dewey's reconstruction of metaphysics with special emphasis on his principle of continuity. Natural continuity is then discussed in the metaphysics of experience and learning, demonstrating how learning is the growth of situations through the realization of individual interest. This process is fundamentally aesthetic in nature. Philosophy serves as a method of remaining aesthetically receptive to our world to critically evaluate how we inhabit it. Learning is itself art, or, a process of aesthetic appreciation and production, through which we participate most vitally and directly in a communion with our cultural and natural world.

From this point of view, the commonplace dualisms of contemporary education are critiqued, and the import of a transactional, learning-centric paradigm of education is discussed. Namely, it is argued that learning, understood as inhabitation, is the life process itself, and is fundamentally autotelic in nature. Understood in this way, learning represents our most basic point of contact and expression of the world or ecosystem. It is concluded that not only must learning situations be allowed to determine their own meanings to as great a degree as possible, but also that this is a condition for a democratic and ecologically conscientious education and inhabitation of the Earth.

<#ifndef CMARK>
**Keywords:** *John Dewey, Thomas Alexander, Inhabitation, Aesthetic Experience, Autotelic Learning, Ecological Humanism, Transaction, Learning Situation*
<#endif>
